# Node Auth App
### User authentication and authorization in node.
* Built using Express, Joi, bcrypt, jsonwebtoken and mongoose;
* Remember to run `npm install` before using it;
* Run `npm start` to run the server.


# API Endpoints (usage info):
### POST "/register"
This public endpoint expects the following data:
1. **name:** a string containing the _name_ of the user that is being registered
2. **email:** a unique string containing the _email_ of the user that is being registered
3. **password:** the password for the user that is being registered

The return data is the registered user object.
### POST "/login"
This public endpoint expects the following data:
1. **email:** (string)
2. **password:** (string)

The return data is a jwt containing the user object as the payload crypted with the secret word in config.json
### POST "/getUsers"
This protected endpoint expects the following data:
1. **token:** (string) the jwt of an admin user (can be obtained by asking to the "/login" endpoint)

The return data is an array containing all the users in the db.
### DELETE "/deleteUser"
This protected endpoint expects the following data:
1. **token:** (string) the jwt of an admin user (can be obtained by asking to the "/login" endpoint)
2. **\_id:** (string) the id property of the user that needs to be deleted

The return data is the deleted user object.
### POST "/getUser"
This protected endpoint expects the following data:
1. **token:** (string) the jwt of an admin user (can be obtained by asking to the "/login" endpoint)
2. **\_id:** (string) the id property of the user that you are looking for

The return data is the user found object.
