"use strict";

const router = require("express").Router();
const userController = require("../controllers/user.controller");

router.post("/register", userController.register);
router.post("/login", userController.login);
router.post("/getUsers", userController.getAll);
router.delete("/deleteUser", userController.delete);
router.post("/getUser", userController.getOne);

module.exports = router;
