"use strict";

const config = require("./config.json");

const port = 60901;
const mongoose = require("mongoose");
const bodyParser = require("body-parser");

const cors = require("cors");

const userRouter = require("./routes/user.routes");

mongoose.connect(config.mongoEndpoint, { useNewUrlParser: true });
const db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => console.log("DB Connection established"));

const app = require("express")();

app.use(bodyParser.urlencoded({ extended: false }));

app.use(cors());

app.set("title", "Auth App");

app.use("/", userRouter);

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
