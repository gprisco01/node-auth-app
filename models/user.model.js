'use strict';

const mongoose = require("mongoose");

const Users = new mongoose.Schema({
  name: String,
  email: String,
  password: String,
  isAdmin: Boolean,
});

module.exports = mongoose.model("users", Users);