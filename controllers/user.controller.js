"use strict";

const bcrypt = require("bcrypt");
const Joi = require("@hapi/joi");
const jwt = require("jsonwebtoken");
const UserSchema = require("../models/user.model");
const config = require("../config.json");

const UserJoiSchema = Joi.object().keys({
  name: Joi.string()
    .alphanum()
    .min(3)
    .required(),
  email: Joi.string()
    .email({ minDomainSegments: 2 })
    .required(),
  password: Joi.string().required()
});

const LoginUserSchema = Joi.object().keys({
  email: Joi.string()
    .email({ minDomainSegments: 2 })
    .required(),
  password: Joi.string().required()
});

exports.register = (req, res) => {
  const { name, email, password } = req.body;

  //Validate the user object with joi before manipulating data
  const validation = Joi.validate(
    { name, email, password },
    UserJoiSchema
  );

  if (validation.error === null) {
    //Check if a user with the same email already exists
    UserSchema.findOne({ email }, (error, result) => {
      //if not, continue with the registration
      if (!result && !error) {
        //Generate a key to hash the password
        bcrypt.genSalt(config.saltRounds, (error, salt) => {
          if (error) {
            res.send(JSON.stringify(error));
          } else {
            //Hash the password with the key (salt)
            bcrypt.hash(password, salt, (error, hash) => {
              if (!error) {
                const user = { name, email, password: hash };

                //If there are no users in DB, the first to be registered has admin privileges
                UserSchema.find((error, result) => {
                  if (error) {
                    res.send(JSON.stringify(error));
                    return;
                  }

                  if (Object.keys(result).length < 1) user.isAdmin = true;

                  //Add the user in the db
                  UserSchema.create(user);
                  res.send(JSON.stringify(user));
                });
              } else {
                res.send(JSON.stringify(error));
              }
            });
          }
        });
      } else if (result) {
        const error = "The user is already registered";
        res.send(JSON.stringify({ error }));
      } else if (error) {
        res.send(JSON.stringify({ error }));
      }
    });
  } else {
    res.send(JSON.stringify(validation));
  }
};

exports.login = (req, res) => {
  const { email, password } = req.body;

  const validation = Joi.validate({ email, password }, LoginUserSchema);

  if (validation.error === null) {
    //Retrieve data
    UserSchema.findOne({ email }, (error, result) => {
      //If the user is found, compare the crypted passwords
      //to see if the given password is correct
      //Else, send an error message
      if (!error && result) {
        const user = { ...result._doc };

        bcrypt.compare(password, user.password, (bcryptError, isTrue) => {
          //If the password is correct, return a token
          if (!bcryptError && isTrue) {
            const token = jwt.sign(user, config.salt);
            res.send(JSON.stringify({ jwt: token }));
          } else {
            res.send(JSON.stringify({ error: bcryptEerror }));
          }
        });
      } else {
        res.send(JSON.stringify(error));
      }
    });
  } else {
    res.send(JSON.stringify(validation));
  }
};

/*
 * For these CRUD operations, we always decode the jwt, check if the
 * user is admin and perform the operation, otherwise we send an error message
 * to alert the user that he's not admin
 */

exports.getAll = (req, res) => {
  const { token } = req.body;

  //Decode the jwt and get user info
  const user = jwt.decode(token);

  //Check if the user is admin (this is a protected endpoint)
  if (user && user.isAdmin) {
    UserSchema.find((error, result) => {
      res.send(JSON.stringify(error || result));
    });
  } else {
    res.send(JSON.stringify({ error: "The user must be admin" }));
  }
};

exports.delete = (req, res) => {
  const { token, _id } = req.body;

  const user = jwt.decode(token);

  if (user && user.isAdmin && _id) {
    UserSchema.findOneAndDelete({ _id }, (error, result) => {
      res.send(JSON.stringify(error || result));
    });
  } else {
    const error = "The user is not admin or the _id is not valid";
    res.send(JSON.stringify({ error }));
  }
};

exports.getOne = (req, res) => {
  const { token, _id } = req.body;

  const user = jwt.decode(token);

  if (user && user.isAdmin && _id) {
    UserSchema.findOne({ _id }, (error, result) => {
      res.send(JSON.stringify(error || result));
    });
  } else {
    const error = "The user is not admin or the _id is not valid";
    res.send(JSON.stringify({ error }));
  }
};
